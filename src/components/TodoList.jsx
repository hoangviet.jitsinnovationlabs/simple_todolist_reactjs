import React from "react";
import { useState, useEffect,useRef } from "react";
import "./TodoList.css";
import AddTodo from "./addTodo";
const { forwardRef, useImperativeHandle } = React;

const TodoList =(listTodo) => {
  const [task, setTask] = useState("");
  const [render, setRender] = useState(false);
 let list = JSON.parse(localStorage.getItem('todoList'));// danh sách todolist
  console.log(list)
  let input;

  // localStorage.clear()
  const handleOnchange = (e) => {
    setTask(e.target.value);
  };
  const handleDeleteClick = (e, id) => {
    localStorage.setItem("todoList",
    JSON.stringify(list.filter((listItem) => listItem.id !== id)));
    setRender(!render)
  };
  const handleCompletedClick = (e, id) => {
    e.preventDefault();
    list.map(item=>{if(item.id==id) item.completed=!item.completed})
    localStorage.setItem("todoList",
                  JSON.stringify(list))  
    setRender(!render)
  };
  useEffect(()=>{
    list= JSON.parse(localStorage.getItem('todoList'))
    
  },[list])
  return (
    <> 
      {list !== [] ? (
        <ul className="list-todo" style={{ marginTop: "30px" }}>
          {list &&list.map((aitem, index) => {
              return (
                <>
                  <div key={index} className="list-todo-item" >
                    <li
                      className={
                        aitem.completed == false ? "task item-padding" : "task item-padding task-completed"
                      }
                    >
                      {aitem.title}
                    </li>
                    <div
                      className="item-state"
                    >
                      <button
                        className="completed"
                        onClick={(e) => {
                          handleCompletedClick(e, aitem.id);
                        }}
                      >
                        Completed
                      </button>
                      <button
                        className="delete"
                        onClick={(e) => handleDeleteClick(e, aitem.id)}
                      >
                        Deleted
                      </button>
                    </div>
                  </div>
                </>
              );
            })}
        </ul>
      ) : null}
    </>
  );
};

export default TodoList;
