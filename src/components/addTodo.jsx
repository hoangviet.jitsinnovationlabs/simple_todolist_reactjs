import React,{useState,useRef} from 'react'



const AddTodo = (props) => {
   

    var listItem
    let input;// get data from submit form
    const addTask = (e) => {
      if(!input.value.trim()) return ;
          listItem = JSON.parse(localStorage.getItem('todoList'))
          ==null?[]:JSON.parse(localStorage.getItem('todoList'));
        e.preventDefault();  
            const taskDetail = {
              id: Date.now(),
              title: input.value,
              completed: false,
            };
        listItem.push(taskDetail)
        localStorage.setItem("todoList", JSON.stringify(listItem))
        props.reRender();
       
}
    
  return (
    <div className="input">
    <form onSubmit={addTask}>
      <input ref={node=>input=node}/>
      <button type="submit"  className="button">
          Add
      </button>
    </form>
    
  </div>
  )
}

export default AddTodo