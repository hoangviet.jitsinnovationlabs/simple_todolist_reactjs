import logo from './logo.svg';
import './App.css';
import TodoList from './components/TodoList';
import AddTodo from './components/addTodo';
import {useState} from 'react'
function App() {
  const [render, setRender]=useState(false)
  const Rerender=()=>{
    setRender(!render)
  } 
  return (
    <body>
       <div className="container">
              <header>
                <h1>Todolist</h1>
              </header>
              <AddTodo reRender={Rerender} />   
              <TodoList/>
              
        </div>
    </body>
   
  );
}

export default App;
